



#Script Start
label day_4:

if ikarineS >= 1:

    jump day_4_no_sleep
else:

    jump day_3_sleep

label day_4_no_sleep:

scene bg classroom_window
with fade

"This is different than just being alone. I'm alone with the room itself."
"I may be the only person in the entire building..."
"which isn't actually anything more than the room."

show window

"The tree wall in the distance is gently rustled by the early morning wind. The branches wave back and forth with the stronger gusts."
"Closer, the individual trees placed across campus seem more agitated by the wind than their distant brothers."
"The lone tree is shook to the core by a gust of wind, but a tree alongside others barely shakes."

na "Suke, you're here quite early. I hope you understand that doesn't make up for your absence yesterday."

hide window

show nagamura normal

na "I take absences very seriously in my class. Especially so seeing you haven't even been here a week yet."

s "I understand, sir."

na "I don't know how you behaved at your old school, but let me tell slacking off won't pass here."
na "My class is not easy, Suke. I don't know if you've noticed, but I assign what some call 'an inordinate amount' of homework."
na "If you don't attend my class you will not learn the tools necessary to complete it on time."

s "Sir, if I might add, I've already finished the homework for this week-"

na "Ah, so you're not a total lost cause then."

s "-and for next week."

na "I see. And is it ready to turn in."

s "Here you go."

na "..."

"The room returns to silence as Nagamura pours over the first few pages of Suke's homework."
"He pauses and raises his eyebrows at a particularly large problem, solved in a comparatively small amount of steps."

na "Suke, is this work your own?"

s "Of course, sir."

na "Be honest with me. Look at this here, we haven't even introduced third-dimensional integrals in class. Yet somehow you've done this perfectly in three steps."

s "I don't know what to tell you, sir. It's not too difficult, once you know how to do an integral you can do them all."

na "How long did it take you to finish?"

s "I finished it last night, after skipping the afternoon session."

na "Suke, exactly how bored are you in my class?"

s "I don't think I should really say. I don't know, I'd say I'm pretty bored I guess."

na "A good education is necessary for every student, and the purpose of school is to give you an education, not waste your time."
na "Now, obviously you won't be learning anything sitting in my class, nothing at all with a brain like that."
na "And I just can't switch my entire curriculum around, everyone else would get lost. So let's strike a deal."
na "I'll let you skip my class when you feel like it. No penalty, grade demerits. But in return you have to stay at least 2 weeks ahead on homework assignments."
na "And most importantly, every day right after our morning session you have to show me that you've learned something new."

s "'Learned something new'? From further ahead in the book or?"

na "Anything, a new skill, a talent. Read about the American and Russian Cold War. Learn how to compute eigenvalues. Learn a new instrument."
na "You'll know if you've learned something before you show me."

"Wow, I've never had an offer like this before. He's giving me the bachelor life on a golden platter."

na "And of course, I expect you to participate in class at least every now and then for tests, quizes, group projects and such. I wouldn't want you to become a loner."

s "I don't know what to say. It's a great offer, can I get back to you later?"

na "Yes, but until you accept my offer understand that any further absences will not go unpunished."

"Nagamura turns to sit at his desk and goes over the classwork for the day."
"What an offer. I can't believe any teacher would actually agree to that. Does he really trust me that much? Does he think I can teach myself?"
"Do I think I can teach myself? The only reason he offered anyway was because I missed class. And I only missed class to lay about on the floor."
"Am I really going to get to skip school just so I can lay on the floor?"
"Students start trickling in and I lost my train of thought to the new din of conversation."

show mako normal
with fade

mm "Suke, you're here! Mako was worried when you didn't show up yesterday."

show mako normal
with close

mm "Actually, Mako wasn't too wooried. She knew you and Ikarine would get along."

s "Yeah, apparently we have the same interests: nothing."

mm "Now don't you be a bad influence on Ikarine. She needs to study and go to class, she is going to do something with her life."
mm "And you don't get to do stuff with your life by sleeping away the day."

s "How do you know I was with Ikarine anyway?"

mm "Oh, that's simple! Ikarine told me. She told me you slept on her floor and left before she woke up."
mm "Ikarine thought it was very rude to leave without waking her up first. What if a burglar snuck in or something?"

s "A burglar? And how would it help if I had woken her up."

mm "Well, if Ikarine was sleeping and didn't know you had left she might disregard unusual sounds, burglar sounds, because they might be coming from you."
mm "Very dangerous: to not know who's in your room. You should be more careful Suke."

s "It's weird, but I actually see your point."

mm "Why is that weird?"

s "Shh, I think class is about to start."

scene bg black
with fade


#Day 4 sleep
label day_4_sleep:




"Well, Nagamura certainly didn't try to make his lectures any more interesting."

label day_4_rejoin:

scene bg campus_classroom
with fade

"I step out of the classroom into a cool breeze." 


if kateS >= 2:
    jump kateLunch1

label kateLunch1:

show mako normal
with fade

mm "Hey Suke, you want to come to lunch with Mako?"

s "Hey, I'm sure it would be fun, but I already told Kate that I'd eat lunch with her instead. See you later in class."

mm "All right, Suke, but you better eat lunch with me sometime soon. Mako doesn't like it when you cheat on her too much."

s "Uh sure."

hide mako

"Mako takes off in a rush towards the dining hall. That girl sure does value her nutrition."
"Overhead the sky is a nice shade of blue. A light blue that clashes perfectly with the horizon."
"There's a steady wind keeping it cool outside and intermittent gusts which push around the fallen leaves."
"It's hard not to appreciate an open campus on days like this."
"I spend so much time reveling in the outside weather I almost forget that I have a lunch date with Kate."
"A lunch get-together, not a date."
"At least I don't think it's a date."

scene bg cafeteria
with fade

"Kate's waiting for me just inside the door."

show kate energetic
with fade

ka "Battle brother! There you are, I was afraid that you had abandoned me-er I mean abandoned our mission."

s "Oh, and what mission is that?"

ka "The most important mission of all, acquiring sustenance!"

s "Well we better hurry. I haven't been late to the lunch line so far, I don't want to break that streak."

ka "Away!"

"Kate and I get our meals, a porkchop for her and some udon for me."

ka "Let's eat!"

"I eat faster than I normally do in an attempt to keep up with Kate."
"It would be rude to let her eat twice the food I have on my plate twice as fast as me."
"Despite my best efforts she beats me to the finish line by a minute at least."
"She uses her early finish advantage to start question me while my mouth's full."

ka "So Suke, I never asked you, did you play any tabletop RPG's before coming to Bedaio?"

"I quickly slurp down some noodles before answering."

s "Nope, though I did play Monopoly once or twice ."
s "Pretending to be a rich person took a lot of imagination."

"It's a good thing that Kate already finished her lunch because if she had anything in her mouth she would have spit it out."

ka "Bahahahahha!! That's a good one battle brother. I guess everything can be an RPG if you take it seriously enough."

s "How about you?"

ka "Me, how about me what?"

s "Did you play? Tabletop RPG's did you play any before Beadio?"

ka "Oh yeah definitely! Back in elementary school, all the neighboring senpais were into Dungeons and Dragons and they would invite me over to play with them."
ka "They would have me play minor characters or squires sometimes because they thought I didn't have the attention span to make my own character."

s "Were they right?"

ka "Oh yeah totally. I had trouble keeping track of the most basic things about my bit characters."
ka "DnD is too slow-paced and complicated for little kids to play well. It was a great introduction though."

s "With how fast you wolfed down that porkchop I don't think you've become too slow-paced yet."

ka "One must always eat fast because when one is taking a meal they are most vulnerable."
ka "Just look at yourself now, somebody could sneak up behind you while you're so focused on your noodles and you would never know it."

s "You see I'm like a ninja. You may think I'm not paying attention but I see everything. You see that kid over there?"

ka "The one by the lunch line?"

s "Yeah, he's going to drop his plate in T minus five, four, three, two ..."

ka "... Nothing happened."

s "But if it did, I would have been ready. You know, if some of it was going to spill on me or something."
s "I'd be totally ready."

ka "hmmppphh, I'm sure you would Suke."

s "I think you finish your food because you're afraid to laugh with your mouth full."

ka "Why of course not. I'll have you know that as a warrior I've perfected the art of stifling a laugh."
ka "It wouldn't do to blurt out in laughter in the middle of a battle."

s "But a good laugh would strike fear into the hearts of your opponents."
s "Especially a laugh like yours."

ka "And what exactly is wrong with my laugh?"

hide kate

scene bg black
with fade

"Kate and I joked around for the rest of lunch before we both went our separate ways to afternoon lessons."

scene campus_classroom
with fade













