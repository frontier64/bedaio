#Day 2

#Character definition
define na = Character("Nagamura", color = "#700000")
define mm = Character("Mako", color = "#dd0066")
define li = Character("Lisa", color = "#FF0000")
$ makoS = 0

#Image definition

#Backgrounds
image bg campus_cafeteria = "images/background/campus_cafeteria.jpg"
image bg campus_1 = "images/background/campus_1.jpg"
image bg campus_classroom = "images/background/campus_classroom.jpg"
image bg campus_dormitory = "images/background/campus_dormitory.jpg"
image bg cafeteria = "images/background/cafeteria.jpg"
image bg classroom = "images/background/classroom.jpg"

#Lisa
image lisa startled = "images/Lisa/lisa_v1.jpg"
image lisa normal = "images/Lisa/lisa_v1.jpg"
image lisa shy = "images/Lisa/lisa_v1.jpg"

#Script start

label day_2:

    s "Ahhh!!!!"
    s "What the. Oh, it was just a dream. I.... oh."
    "..."
    "I better go take a shower and get dressed. It’s the first day of classes. Well, it’s the first day for me at least."

    scene bg campus_classroom
    with fade

    "I’m greeted by a teacher in the hallway outside my homeroom. "
    p "Hello, you’re a new face. Ah yes, they told me I was to have a new transfer student coming in today. Suke Matoishi? I’m Mr. Nagamura, nice to meet you."
    s "Nice to meet you too Mr. Nagamura. I look forward to learning in your class. "
    na "Yes, I’m sure you are. Seeing today is your first day at Bedaio, do you want to introduce yourself to the class?"
    "Do you want to introduce yourself?"
    menu:
        "Introduce myself":
            $ makoS += 1
            jump introduce_myself
        "Just take my seat":
            jump take_seat

label take_seat:
    s "Thank you for asking. No, I would prefer if you told the class my name and let me take my seat. I get very stressed when I'm singled out."
    na "I see, I'm glad you told me. I'll try not to put you in any situation that might make you feel uncomfortable."
    s "Thank you."
    "The teacher opens the door and I walk into my new homeroom behind him. "

    scene bg classroom
    with fade

    "The students all perk their heads up as the teacher walks in, and then look up even more as I follow him. A new student coming in halfway through the school year is a pretty unusual occurrence. "
    na "Class, this is Suke Matoishi. He will be joining us for the remainder of the school year. Please give him a warm welcome and make him feel home."
    if kateS + makoS == 0:
        "Icame here to escape from home not to remind myself of it."
    jump introduce_rejoin

label introduce_myself:
    s "I don’t see why not."
    na "I like to give my students the option to participate or not. I see no reason to force shy students to embarrass themselves in front of the class."
    na "We live in a time and place where one can work from home and make more than a man that goes to the office every day. There is no need to force sociability on everyone."
    s "I never thought about it that way before. "
    na "Well come on, most of the students have taken their seat already."

    scene bg classroom
    with fade
    
    "The teacher opens the door and I walk into my new homeroom behind him. "
    "The students all perk their heads up as the teacher walks in, and then look up even more as I follow him. A new student coming in halfway through the school year is a pretty unusual occurrence. "
    na "Class, we have a new student joining us. Please join me in welcoming him. "
    "Now every person in the room is looking at me expectantly."
    "Why did I say I would introduce myself and not actually think of anything to say. "
    s "Hello, I am Suke Matoishi. I’m from Akihabara. Back at my old high school I played on the baseball team. My main hobby is working with computers, I plan to be a software technician one day."
    s "I am looking forward to spending the rest of my high school career at Bedaio!"
    "I think I took that a little too far. The class doesn’t seem to mind though."
    jump introduce_rejoin

label introduce_rejoin:
    na "Suke, you can take the seat on the left, by the window."
    "I look to where Mr. Nagamura is pointing and I see the empty seat he’s talking about. It’s one row from the very back, I guess he doesn’t have too high hopes for me."
    "I get to my seat and settle in. There’s a break before class starts back up while Mr. Nagamura shuffles his papers around. I turn to my right to see the girl sitting next to my leaning over. "
    p "Suke, that’s a great name! And you work with computers, that must be sooooo difficult. Oh, let me introduce myself, I’m Mako Mankanshoku."
    "She offers me her hand. I shake it, she has a pretty firm grip. I take the opportunity to look her over. She has long, died pink hair, a round face. She’s made her school uniform pop by adding a multi-colored silk scarf around her neck and some buttons to her lapel. "
    mm "Are you looking me over?"
    s "Uh, no, I uhh"
    mm "That’s totally cool! I put a lot of effort into looking good. If I wanted people not to look I wouldn’t t try to be so beautiful. I take it as a compliment"
    s "Oh, well, I meant it as a compliment! "
    mm "Suke, you’re so cute. We should get lunch together. Have you made plans with anyone else?"
    s "I don’t even know anybody else at this school,"

    if kateS >= 1:
        s "Except for this one girl Kate, she’s in the table top games club."
    s "I’d be happy to get lunch with you Mako!"
    mm "Great! Ok, I think Nagy~ is starting now, let’s focus!"
    na "Okay class, please get settled. Today our topic of discussion is line integrals."

    "Line integrals? I did those last semester. I guess they teach in a different order here. I find it difficult to pay attention to the lecture because I already know most of it. "
    "Eventually lunch break rolls around. "

    mm "Ok Suke~ are you ready??"

    s "Sure, where are we going to eat, I didn’t bring a lunchbox. So I have to pick up food in the cafeteria."

    mm "Oh, I normally sit in the cafeteria anyway. Very few students choose to eat elsewhere. "

    s "That’s unusual. "

    "Mako and I head out the class and then outside. It’s a little chilly outside. Even though she probably just got it as a fashion choice, Mako has the right idea with her scarf. "
    "I don’t think I’m going to get used to these separated school buildings anytime soon."
    "Mako and I walk across the campus to the cafeteria. The same building I heard the piano in last night. "

    mm "Hurry Suke! We have to get in line quick so we can get the good options before they’re all gone!"

    "Mako takes my hand and pulls me into the cafeteria building. She drags me all the way to the end of the line."

    mm "Aww, we’re too far back in line. Now we’re gonna be stuck with the slop!"

    "Contrary to Mako’s prediction, there’s still plenty of options when we get to the front of the line. We get our food and head to a table. "
    "We’re in the middle of the dining hall, right by the piano. Sadly, there’s nobody playing it now, everybody is too busy eating."
    "Mako tries to make conversation inbetween mouthfuls of food. "

    mm "Suke *gulp* why did you decide to *swallow* come to Bedaio?"

    s "There were a lot of reasons. I couldn’t remain at my old school and I wanted a major change. I thought a boarding school like Bedaio would be just what I needed."

    mm "So, are you liking it here so far? I remember when I first came to Bedaio I was so scared. I never lived away from my parents before."

    mm "Sleeping on my own, having to force myself to study, just everything, it’s so different right? "
    mm "I was afraid I wouldn’t be able to handle it all. But Mako’s still here isn’t she!"
    mm "Now I’m glad because once I finally graduate I’ll already have experienced what it’s like to live alone and I’ll be ready."
    mm "Oh but please do tell, how are you taking it all?"

    s "I haven’t really thought about it much. I’m just glad I don’t have to cook. I can’t make anything more complicated than a rice ball. "

    mm "Everybody gets tired of cafeteria food eventually if that’s all they have to eat. You’re going to have to learn cook at some point."
    mm "Unless you find a cute girl to cook for you! Hmm yes, Mako thinks that route might be easier for you if you’re really that horrible at cooking."
    s "Hey, I’m not that bad a cook! And I don’t see how I could get tired of cafeteria food if it’s going to be this good everyday."
    mm "You’ll see. Why don’t you hurry up and finish? I want to walk around a little before we go back to class."
    "We each scarf down the rest of our lunch without further conversation. After we’re done Mako leads me back outside. "
    mm "The weather is lovely this time of year. Our campus is on top of a hill you know! "
    s "I heard"
    mm "We get the best of every season. In the winter we get plenty of snow. In the summer, plenty of sun. And there’s never any flooding in the rainy season because it all runs downhill."
    s "Sounds like a veritable paradise."
    mm "Oh and in autumn the trees turn the cutest colors. I just love it here."
    "Mako and I walk around the campus. I’m totally lost, but I trust she knows well enough where we are to get us back before the bell rings. "
    "Groups of students are leaving the cafeteria and entering some of the school buildings around us. One student catches my eye. She looks incredibly familiar."
    "It’s not until she’s out of sight that I remember where I know her from."
    s "That girl from the plane"
    mm "hmm? Who from the plane?"
    s "Some girl, walking around just now, I thought she looked familiar and I just remembered where I saw her. "
    menu:
        "I wanted to talk to her again.":
            $ megumiS += 1
            jump megumi_talk_again
        "I only remember her because she was so weird.":
            jump megumi_is_weird

label megumi_talk_again:
    s "I wanted to talk to her again."
    mm "Oh-"
    "Mako is cut off by the bell signaling that classes will resume shortly. "
    mm "Well we better hurry back to class! I’m sure you’ll see her again later."
    "Mako grabs my hand again and drags me along back to class. The afternoon period passes as uneventfully as the morning period. "
    "Class lets out and I find myself in the hallway. Mako has already run off somewhere so I find myself bored with nothing to do. I head off to go look for that girl I met on the plane. "
    "I leave my school building, which I just learned is called “Village East”, and head to where I think I saw her. "
    "Sure enough I spot her as she is walking out of a building. I walk over to here before I can really think of something to say."
    s "Uh hi, remember me?"
    me "Oh, so my suspicions were correct. Still this is an interesting turn of events. I didn’t expect you to seek me out of your own volition."
    s "What are, you knew we were both going to Bedaio? Why didn’t you say something on the plane?"
    me "What would have been the point of that? Either we were both going to the same school or we weren’t. If I had asked it would only have served to ruin this moment."
    me "And this moment is most interesting moment I’ve experienced all day. "
    s "I guess."
    "I have nothing to say. What can I say? I don’t know anything about this girl, but I definitely want to learn more. "
    s "What are you up to now?"
    me "Classes are over, if today was a good day I would be able to find a nice spot and write, but today is not a good day. "
    me "I have to go to a book signing in town. It’s nothing major, I’m personal friends with the store owner and he thinks stuff like this helps me out. "
    me "I find these events such a bore. There’s never enough to have a real conversation with anybody. They say hi, I respond in kind and sign their, I mean my, no "

#Convert the <i>'s to actual italics'
    me "Their, I sign <i>their</i> book and send <i>them</i> on <i>their</i> way."
#The line above
    me "Actually, since you’re not doing anything, can you go with me? Maybe I can talk to you to avoid talking with the signees. "
    me "I’ll even buy you dinner afterward. Why am I even asking you of course you’ll come. You get to sit with a famous author at her book signing and then eat a free dinner later. Who would say no?"
    me "I’ll meet you at the front gate in twenty minutes. That’s enough time to put your book bag away and get ready, however you get ready."
    "Megumi doesn’t give me time to respond as she walks away to  one of the girls dormitories. I have to use my pocket map of the school to get back to my own dormitory. "
    scene bg black
    "..."
    scene bg bedaio
    "I arrive at the front gate exactly on time. I see Megumi walking up a little bit after me. "
    me "Ah, you’re a little early. You wanted to make sure you weren’t late but you didn’t want be too early because of all the things that would implicate. "
    s "Actually, I’m perfectly on time, you’re the one that’s late. "
    me "I don’t see how I could be late to a meeting that I set the time for. "
    s "How far is the bookstore?"
    me "It’s at the edge of town closest to Bedaio, once we get down the mountain we’ll be nearly there. A twenty minute walk tops. "
    me "Our dining location is just a little bit beyond the bookstore. Another ten minute walk. "
    s "Oh, you already picked out where we are eating? What if I have some special diet? Maybe I can only east sushi?"
    me "Maybe I picked a sushi shop. "
    s "What if I’m allergic to seafood?"
    me "Maybe I didn’t pick a sushi shop. And I would feel very sorry for you if you couldn’t eat seafood. The consumption of sushi is one of life’s greatest pleasure."
    s "Indeed it is. So when are we going to get to the bookstore?"
    me "We’re still on the mountain. When we are off the mountain we will be near the bookstore. I’ve already told you this once Suke, why must you bore me further with incessant questions about our destinations?"
    s "I’m just making conversation. "
    "Megumi and I speak little more as we continue down the mountain. Eventually we get to the bottom, and as Megumi promised the bookstore is right there. "
    "Walking into the store I can see the table setup for Megumi’s book signing. Beside the table is a stack of books and behind Megumi’s seat there’s a large banner with her name on it."
    s "Wow, you really are a celebrity aren’t you."
    me "Wait until you see how many people show up before you offer a remark like that. "
    me "Wait here a second, I’m going to ask the store manager to bring out another seat. "
    me "You can look over the store’s selection I guess. Oh, and they sell Bedaio schoolbooks if you’ve yet to buy yours. "
    "I already have all my textbooks so I make my way to the science fiction section. They offer a wide array of novels. I find myself browsing from title to title without giving any book a chance. "
    "I eventually find a book that piques my interest. It’s about a human raised by Martians and his struggles after coming to Earth for the first time. "
    "The writer’s style is incredibly engaging. I read the first twenty pages standing in front of the bookshelf. I’m not sure how much longer I would have been stuck there if Megumi didn’t wave me over to her table. "
    "We both take our seats at the table, Megumi front and center with me covering her left flank. I quickly re-engross myself in my book. "
    "Soon enough groups of patrons queue up in front of our table and Megumi begins signing her autograph at a rapid pace."
    me "Thank you so much for your interest in my work. I appreciate each and every reader. Be sure to look for my upcoming book “A Stranger Life” coming out in December!"
    "She repeats the same line verbatim to each person that brings her a book to sign. "
    "I begin to drift out of our reality and into the designed by the novel resting in my hands. The constant drone of Megumi and her fans serve as an incantation to drive me deeper into a world of fantasy."
    "After another seventy five pages or so I am dragged back to reality by a hand on my shoulder."
    me "Hey Suke are you here? Earth to Suke, beep boop."
    s "Oh, wow, you can say something else besides “Be sure to look for my-"
    me "All right, all right. A lot of hlp you were. You were supposed to be my distraction, liven things up when it got too boring. I forgot you were even sitting next to me by the end of it."
    s "Sorry, I got really engrossed in this novel. "
    me "Let me see, yes, that’s a classic, really defined sci-fi in the 70s-80s. Well come on. I’ll meet you by the entrance, I just have to say bye to the storeowner."
    "Megumi gets up and walks off to the back of the bookstore. I grab my book and make way for the entrance. "
    "When Megumi’s done talking she and I leave the store and head off through town in a path chosen exclusively by her. We stop outside a small restaurant in the middle of a block. "
    "We walk through the entrance before I think to read the restaurant’s name. The lighting inside is significantly dimmed. I can barely make out the hostess. "
    "Apparently Megumi’s eyes are quicker to adjust than mine and she takes the lead with the hostess. I find myself following along from the back. "
    "When we are seated my eyes finally grow accustomed to the dim light."
    s " This place is really fancy. I don’t think I’ve ever been in a restaurant like this before. "
    me "It’s an Italian place. Not very common. I love Italian food so having this place close to campus is a godsend. I try to eat here at least once a week. The food is just so good. "
    s "How did you develop a taste for Italian food?"
    me "That’s a long, uninteresting story. I’ll tell you sometime later when we don’t have anything more interesting to talk about. "
    "We’re interrupted when the waitress takes our drink orders. I just get water, but Megumi orders something with an English name."
    s "An “Ahnuld Pahner”? What’s that?"
    me "Hahaha, I ordered an “Arnold Palmer”. It’s just ice tea and lemonade. "
    s "How did the waitress know what you meant?"
    me "I told you, I eat here at least once a week. It would be shocking if she didn’t know what an Arnold Palmer is by now. "
    s "You really eat here once a week? Doesn’t that get, expensive? This place looks very fancy."
    me "Don’t worry Suke, I’ll pick up the tab."
    s "No, that’s not what I meant, just nevermind. "
    me "I know what you meant. Suke, I’m an author with books in stores over half of Japan. I would eat here every day if schoolwork and my writing allowed it. "
    s "I’m sure you’d get sick of Italian food if you ate here every day. "
    me "There’s a lot of variety. Here, stop talking to me and check out the menu, I want order when the waitress brings our drinks. "

    "I open the menu for the first time and read it over. I can’t tell if there’s any variety or not because I have no idea what anything is on this menu."

    s "Umm Megumi, I’ve never eaten Italian befo-"

    me "Oh yeah, what was I thinking. Don’t worry, I’ll order for you. "

    s "Try not to get me anything too out there. What’s the typical Italian fare? Pizza and noodles right?"

    me "Yes, but not they’re not normally served together. Just wait and see. "

    "I continue to look over the menu even though I won’t be ordering anything. Eventually the waitress comes back with our drinks and takes our orders. "

    me "Thank you, we’ll be ordering now. I’ll have the pasta pomodoro.... Yes and I’ll be ordering for him as well. He’ll have the lasagna frita. "

    s "Ooh, a “Lasagna Frite” that sounds fancy. "

    me "It’s just some fried pasta, meat sauce and cheese."

    s "Well then I guess I’ll have to try some of your Pasta Pomodoro. We can share right? That’s not like frowned upon in Italian restaurants is it?"

    me "Sure we can share, at forty to twenty. "

    s "Forty to twenty what?"

    me "I get forty percent of your meal and you get 20 percent of mine. "

    s "Whoah whoah whoah, that doesn’t sound horribly fair. How about thirty to thirty. "

    me "I’ll go twenty to thirty. "

    s "Come on, I’m the bigger one here, I need more sustenance than you do. I should be the one arguing for a larger portion. "

    me "Your body may require more nutrition than mine, but writing requires a lot of mental effort. I’m sure I burn at least three times the brain calories that you do."

    s "Well regardless, I’m not going any higher than thirty to thirty. "

    me "Ok, but you have to give me an extra breadstick. "

    s "Deal"

    "With that settled the waitress arrives bringing our food."
    "Megumi hastily eats as much of my meal as she eats of her own. It actually disappoints me a bit as I’m really enjoying it. "
    "Despite my best efforts Megumi comes out on top with a solid forty to thirty lead. "

    s "That was fantastic"

    me "Eh, the pasta was a little dry."

    "The waitress comes back one final time to take our plates away and give us the check. Before I am able to even reach over Megumi has her credit card out and hands it to the waitress. "

    me "You jumped a little there, Suke. Have you never had your meal paid for by a girl?"

    s "Just my mother. And then only very reluctantly."

    me "I’m sure she was happy to do it. Your company has more than made up for your meal. "

    s "Being this charming takes a lot of work you know. "

    me "For you, I’m sure it does."

    "Megumi and I get up from our table and walk out the front door. Enough time has passed that our eyes don’t need to adjust too much to the sunlight."
    "We exert ourselves walking back to school much more than we did on the way down. I guess that’s how it is with schools on hills."
    "We finally make it back to campus just as the sun is beginning to set. "

    me "Thanks for coming with me Suke. Going out like this is a lot more fun when it’s with an interesting person. "
    s "I agree, see you tomorrow?"

    me "Time will tell. Good night, Suke."

    "With that Megumi walks off to one of the girl’s dormitories and I head off to my room."

    scene bg black
    with fade

    jump day_3

label megumi_is_weird:

    s "I only remember her because she was so weird."

    "An autumn breeze chills me and Mako to the bone."
    "The calm scene is broken by the bell signalling the start of afternoon classes."

    mm "Come on Suke, let's get back to class. Nagamura's afternoon lecture is going to be so intersting."

    "Mako pulls grabs me by the hand and pulls me through a twisting maze of school buildings until we arrive back at our class with but a minute to spare."
    "We move to the back of the class and take our seats." 

    hide mako

    scene bg window
    with fade
    
    "I waste thirty seconds before class begins looking out the window."
    
    na "Let's get back to our earlier discussion on line integrals."
    
    "Nagamura's afternoon lecture proved about as interesting as I expected it to be."
    
    scene bg black
    with fade
    
    "..."
    
    scene bg classroom
    with fade

    "After class ends I wait for everybody else to leave before making my own way to the hallway."

    scene bg hallway
    with fade

    "Mako's already run off somewhere so I find myself with nothing to do."
    "I'll go finish up my work for the week in the cafeteria. There won't be a crowd for dinner for at least another few hours."

    scene bg campus_classroom
    with fade

    "The campus is full of students milling around. Most of them appear to be going to clubs or sports activities."

    scene bg campus_1
    with fade

    "The crowd makes it even harder to navigate through the disconnected schoolbuildings. Luckily I happen to remember the correct path to the cafeteria."

    scene bg campus_cafeteria
    with fade

    scene bg cafeteria
    with fade

    "The cafeteria's totally deserted, I guess not many students like to study here."
    "I take a seat by a table in the far corner of the large room"
    "All right Nagamura, your lectures are a joke, let's if your homework follows suit."

    scene bg black
    with fade

    "..."

    scene bg cafeteria
    with fade

    "I'm pulled away from my work by the sound of the cafeteria door opening and closing."
    "The echoing sound footsteps break what little concentration I had left."

    show lisa normal
    #with silhouette

    "It's gotten pretty dark outside and I never decided to turn the lights. I can only make out a rough outline of the source of my distraction."
    "To her I must be invisible."
    "She glides across the center of the cafeteria and sits at the piano"

    hide lisa

    #play music lisa_3

    "..."
    "She is absolutely amazing. I didn't know the piano could make music like that."
    "I spend another few minutes reveling in the music and then get back to studying."

    scene bg black
    with fade

    ""

    scene bg cafeteria
    with fade

    "All right Nagamura, I've conquered all your assignments for the next week."
    "And it was her piao playing that helped me get through it all."
    "I pack up my bags and make for the cafeteria's exit."
    "I should say something, I'm sure she gets told she's amazing all the time, but she deserves to hear it again."
    "Then again, maybe I should just slip out and not disturb her."

    menu:
        "Compliment the pianist":
            $ lisaS += 1
            jump compliment_lisa
        "Sneak out":
            jump sneak_out

label compliment_lisa:

    s "You are a fantastic pianist. Thanks for playing, you helped me get through all my schoolwork."

    show lisa startled
    with fade

    "???" "I didn't reaize there was anybody in here. I-I'm sorry if I disturbed you."

    s "No, no, you... it sounded amazing I was enthralled. It helped me focus."

    show lisa shy
    with fade

    "???" "Oh, well t-thank you."

    s "I'm Suke, it's nice to meet you."

    "???" "H-Hi Suke."
    li "I'm Lisa... it's nice to meet you too."

    s "You know, I've heard the piano the past few nights as well. Do you play every night?"

    show lisa startled
    with fade

    li "I p-play when I can"
    li "I think I'll be going now!"

    hide lisa

    s "No please!-"

    "Why did she run off like that?"
    "I guess one really can be too modest."
    "You stupid idiot, Suke. You should have just left and not disturbed her."
    "It's getting late, I better head back home."

    jump compliment_lisa_rejoin

label sneak_out:
    
    "I best just sneak out of here. She looks really enthralled in her playing, I shouldn't break her concentration."
    jump compliment_lisa_rejoin

label compliment_lisa_rejoin:
    
    scene bg campus_cafeteria
    with fade

    ""

    scene bg campus_1
    with fade

    ""

    scene bg campus_dormitory
    with fade

    ""

    scene bg dormroom
    with fade

    "If only Nagamura's class didn't have required attendance, I could stay up and not go to bed until some ungodly hour."
    "Maybe it's a good thing he does."

    scene bg black
    with fade

    jump day_3

    

