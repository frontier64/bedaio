﻿## The script of the game goes in this file.

## Declare characters used by this game. The color argument colorizes the name
## of the character.

define s = Character('Suke', color="#000000")
define an = Character('Anna', color="#FF0000")
define me = Character('Megumi', color = '#009934')
define p = Character('???', color = '#000000')
define t = Character('Typing Girl', color= '#000000')

## Image declaration
image anna normal = "images/Anna/anna_v1.png"
image bg akihabara = "images/background/akihabara.jpg"
image bg akihabara_river = "images/background/akihabara_river.jpg"
image bg black = "images/background/black.png"




## The game starts here.

label start:

    play music "music/calm3.mp3"

## Girl value declaration
    $ megumiS = 0
    $ kateS = 0
    $ makoS = 0
    $ lisaS = 0

    scene bg akihabara

    "It’s a cold night out. The intermittent gusts chill me to the bone. I should have worn more than this little jacket."
    "The streetlights are out on this corner. It makes everything that much darker. I can see what’s behind me, but I can’t make anything out up ahead." 
    "Maybe it’s time to turn around. Maybe it’s time to turn back..."
    show anna normal
    "???" "Hey, Suke!!"
    "Who said that? It sounds like it came from everywhere at once."
    "???" "Silly, It’s just me Anna."
    "Anna, Anna..."
    "Oh yeah, she’s that girl from class 1-2."
    "Why is she calling out to me?"
    an "What are you doing out so late at night Suke?"
    "It’s not that late. Oh, it’s midnight. Maybe it is kinda late."
    s "You’re out pretty late yourself."
    an "Touché"
    "She still hasn’t said why she called out to me."
    "Come to think of it, she hasn’t said much of anything."
    s "I was just going for a stroll. This is a nice part of town isn’t it?"
    "It’s a nice part of town because I normally don’t run into people I know here."
    an "Oh, well I was just doing the same. "
    "She seems hesitant to say more. Why did she stop me if she doesn’t want to talk?"
    "Now we’re both just stopped standing here."
    "Might as well keep going. If she wants to have a conversation I guess she’ll have to walk and talk."
    "It gets brighter as we move into the more developed part of town. "
    "Anna stands off to my right. I catch her staring at me once."
    an "Where are we going now?"
    "Where are we going?"
    menu:
        "I'm heading home.":
            jump heading_home
        "I don't know, still just walking around.":
            jump walking_around
label heading_home:
    s "I’m heading home. I can drop you off on the way."
    an "Oh all right."
    "Now she’s staring at the ground. She’s sad that I’m going home at twelve thirty in the morning?"
    "I can’t even relax now that this girl is awkwardly walking alongside me looking at the ground."
    "What a perfectly good walk ruined."
    an "T-this is my turn. "
    s "Oh okay. Have a good night I guess."
    an "Good night, S-S-S-Suk-"
    "And with that Anna runs takes off down the street."
    s "What’s with her?"
    "At least all that’s done with. I’ll be home soon. "
    "Big day tomorrow. Probably shouldn’t have stayed out so late."
    "At least I’ll be tired for the flight. Maybe sleeping on the plane will help me forget about everything. "
    hide anna
    jump plane_ride
label walking_around:
    s "I don’t know, I guess I’m still just walking around. "
    an "Well, if that’s the case, I know this nice quiet spot by the river. It looks really beautiful this late at night. It’s placed perfectly to reflect the moon at night."
    s "Oh, how far is it?"
    "She just wanted to go for a walk and now she’s taking me to a “quiet spot”. What’s with this girl?"
    an "It’s not far, follow me!"
    "Anna takes off ahead of me at a brisk walk. It’s dark out but she’s not stumbling. "


    scene bg akihabara_river
    with fade
    "After a few more minutes of walking, no make that jogging, I think we’ve found the spot. "
    "It really does look beautiful. It’s a full moon out tonight so the river is as bright as the night sky. "
    "I’m surprised there’s nobody else here but the two of us."
    an "Hey Suke, why don’t you come over here and take a load off?"
    "Anna waves me over to a bench near the edge of the river. "
    an "As far as public benches go it’s pretty comfortable. Although maybe that’s because it’s so cozy out tonight."
    s "So Anna, you come here often?"
    an "It’s a lot more crowded during the daytime. I only like to come here at night."
    an "Most of the time it’s too late to come out by the time I’m done with my homework and dinner. "
    an "I make it out when I can though. It helps me deal... with stuff. "
    s "Oh... what kind of stuff?"
    an "Well, Suke, I-I-Uh Suke... "
    an "Please don’t transfer schools!"
    "That was out of left field. "
    s "How do you know I’m transferring? I didn’t tell anybody at school."
    an "I-I overheard your parents... during a PTA conference. "
    an "They said you were transferring away for good. Why, I don’t know, I don’t want you... I don’t want you to go!"
    s "Why? Why don’t you want me to go?"
    an "I, well I "
    s "You see? This is why I didn’t tell anybody at school."
    an "No, Suke I, let me-"
    s "You don’t want me to go but you can’t tell me why. Do you think I just made this decision willy-nilly?"
    s "Do you think I up and decided to leave one day? No, I’ve been considering this for a year now. "
    "I’ve put more thought into this than everybody else in this world and I know what I need to do."
    an "Suke please, I’m sorry, I shouldn’t tell you what to do. I... "
    an "Please let me think it over first. I had it all in my head and now it’s just gone."
    s "There’s nothing you can say to convince me. And even if you can, even if by some stretch of reality you convince me to stay even I can’t stop it now."
    s "I’m flying out tomorrow."
    "Why am I so angry? I didn’t mean to shout at her. I shouldn’t have shouted at her. "
    "I used her as a conduit to air unrelated frustration. And she took it, she accepted it and even apologized to me."
    "I should tell her I’m sorry. "
    an "Ok, I got it all right in my head now, so I’m just going to say it all at once. Please don’t interrupt."
    an "Suke, I want you to not take your flight tomorrow. I want you to stay at your high school and not transfer away."
    an "I want all this because I like being around you. Even though we’re not in the same class, I have so much fun when we sit at the same table in the cafeteria."
    an "I hope you enjoy those times too. I know you enjoy it to because I see you get the brightest smile on your face."
    an "And I love seeing you smile. I love your smile Suke."
    an "I love you Suke."
    "Wow, that was heavy. "
    "How do I respond to that? She’s cute, but I don’t think of her any special way. "
    "I had no idea she felt this way about me. She’s nothing more than a friend, an acquaintance really."
    "If only I felt something, this should ring some primal instinct in the back of my head to profess my own lover for Anna. "
    "I don’t feel anything though. This is too sudden. This is too, if only she told me before, maybe a month ago. Give me some time to prepare."
    "Hell, if I had a month to think about it maybe I would choose her over moving. But now there’s no decision. "
    "Oh no, I’ve wasted so much time now I don’t have enough time to think of a way to let her down softly. "
    "I have to say something quick, she looks about to burst."
    s "No you don’t."
    s "You don’t love me, Anna. Maybe you’ve been telling yourself you love me and this is your last chance to do something about it, but it’s all nothing."
    s "Think about it, if you loved me all this time would you really wait until the day I was going to leave to say something?"
    s "No, obviously not. If you loved me you would have said something before. The only reason you decided to find me tonight is that I’ll be gone tomorrow."
    s "It’s probably a good thing for you that you got to “confess”. You got to protect yourself from any possible regret. "
    s "Anna, you’re a great girl. This whole beautiful river at midnight thing is real interesting. "
    s "I’m sure you’ll find a cool boy and you’ll bring him down here and sit on this same bench and tell him you love him."
    s "And this cool boy, you know what he’ll say, “I love you too Anna.”"
    s "But I’m not that boy, you rushed yourself and you picked wrong Anna. Now let’s both go home. Come on"
    "Anna’s expression is now entirely blank. It’s as if all her emotion leaked out her ears. "
    "She actually looks a little scary. Well, she’s following me back home so I guess she understood. "
    hide anna
    jump plane_ride



#plane ride

#Image declarations
image bg plane = "images/background/plane.jpg"
image megumi normal = "images/Megumi/megumi_v1.jpg"

#Script
label plane_ride:
    scene bg plane
    with fade
    "I’ve never flown a plane before. I’m not really flying a plane now, I’m just the passenger. "
    "I have no control over what happens. If the pilot decides he’s had enough and wants to end it all there’d be nothing I could do to stop him."
    "What if he just wants to take a vacation off in Hawaii. I could get behind that. "
    "I lean back and realize that I haven’t looked away from the window since we took off. "
    "The woman in the seat next to me got up earlier and hasn’t been back for a few minutes. "
    "Now she’s finally returning, but it’s not her. It’s some other girl..."
    show megumi normal
    "???" "Hi, the lady sitting her before asked me to switch with her, to sit with her husband."
    s "..."
    "Peace be upon me, this girl has the oddest look I’ve ever seen. "
    "Her brown skirt contrasts so heavily with her pink top and green sweater. It’s as if she dressed herself in the dark and didn’t care that none of it matches."
    "She takes a second to look at me before she sits down. She then pulls out her laptop and stares at her screen. "
    "She stares for a second more before she starts typing away. I catch myself staring at her and turn to look out the window once again. "
    "Now the plane is flying by the clouds. The sky outside is momentarily replaced by a solid whiteness when we pass through a cloud."
    "I try to imagine what it would sound like out there, flying at such a high sp-"
    t "Are you running away from home?"
    s "wh-, am I running away from home? what do you mean?"
    t "Did you pack a duffel bag full of your favorite clothes and a picture of your dog, buy a plane ticket with your mom’s credit card, write a goodbye letter to your parents, and then catch this plane to leave it all behind?"
    s "No, no I didn’t do any of that, except I guess pack my bag. I’m not running away from home."
    t "     That’s too bad."
    s "Why would you think I’m running away from home?"
    t "It suits you. You seem very high strung, unsure if you should really be on this plane. "
    t "And the way you look out the window, it’s as if you’re trying to get away from it all."
    t "It all comes together, I mean that and the fact that you’re a kid alone on a plane, and it makes me think you’re running away from home."
    s "Well I’m not. I’m looking out the window because I have a lot on my mind."
    t "That was such a faux-interesting thing to say. Like really, “you have a lot on your mind” you’re just going to say that and not elaborate?"
    t "It could be anything, is it the nuclear launch codes? Do you know the nuclear launch codes and you’re now considering using them?"
    t "Now that would be very interesting, just imagine the mental trauma, an ordinary citizen with the power to wipe out an entire continent. "
    s "Umm, I’m not pretending to be interesting, in fact I think I’m pretty normal. And I definitely don’t know any nuclear launch codes."
    s "I didn’t catch your name by the way. I’m Suke, it’s nice to meet you."
    t "Ooh, what if I don’t tell you my name? Now I know who you are and you know nothing about me. "
    s "You don’t really know much about me, aside from the fact that I’m not running away from home and I don’t know the nuclear launch codes."
    t "A name says a lot about a person. Suke, suke, it doesn’t really roll off the tongue now does it? It’s certainly not a common name either. "
    t "That means your parents must have spent a long time thinking about your name. "
    t "That, combined with the fact that you’re not running away from home leads me to believe that you are an only child!"
    t "It’s right isn’t it Suke."
    s "... You’re right, but I don’t really think your reasoning is sound. "
    t "But if my reasoning is correct it has to be sound. "
    t "Oh, and I’m Megumi. Pretty standard name, doesn’t give you any insight into who I am or anything. "
    t "Would you like to know more? "
    s "I, I think I’m good as it is."
    me "All right then, well if you change your mid just ask!"
    "With that she turns away from me and back to her laptop. Thinking back, she was typing the whole time we were talking. That takes some serious skill."
    "I watch her hands as she click-clacks away. No, “click-clack” is the wrong term, it’s much more graceful than that. Her hands glide over the keyboard, hitting each key in a flowing rhythm."
    "I let my eyes wander up to her screen. She appears to be using some sort of ultra basic text-editor."
    "I try to stop myself, but I can’t help reading as she types. "
    "“...the Runaway and I then ceased communication and returned to our individual activities. Me to my writing, and him, well now, even though we stopped talking our communication continues on in different means. "
    "The Runaway has taken an interest in my writing. I can only imagine what he might think of this. How long will it go on? How long will he put up the façade of ignorance, as if he is merely an outside observer, not one deeply entrenched in the situation he is watchin-"
    s "Hey, stop that."
    me "Stop what?"
    "-g. Oh, now the Runaway has finally decided to speak up.-"
    s "That, what you’re doing right now."
    me "I’m doing plenty of things now. You’re going to have to be more specific."
    "-It is intriguing how quickly he shifts from being totally unabashed to visibly perturbed.-"
    s "Writing, stop writing. That’s not how it’s happening, you’re just writing whatever you feel. "
    me "I can’t write? How can I write what I do not already feel? My writing serves only as an extension of what I already know. "
    me "Is it the tone of my writing that disturbs you? I’m certain I’ll revise it before publishing. "
    me "Although, I am writing unusually well compared to how I normally write on a plane."
    s "It’s not how you’re writing. It’s just, you’re writing about me. You’re writing about our conversation and you’re trying to say what I think but it’s just wrong, the content not the tone."
    me "I’m certainly not writing about you. I’m writing about a Runaway on a plane. You’re not a runaway remember? You told me so."
    "-The runaway is getting heated now, but is having difficulty expressing his own anger. He is much more interesting than I originally thought-"
    s "I’m not "
    me "And I’m not a schoolgirl. Would that words could change reality. "
    me "Speaking of things we’re not, if you’re not a runaway, why are you on this plane?"
    s "I’m transferring schools."
    me "Interesting, I wonder."
    "And with that our conversation is over. The rest of the plane ride passes in relative silence. I look out the window of the plane and Megumi looks through the screen of her laptop. "
    "Somehow I think she has the more interesting view"
    hide megumi

#Image declaration
image bg bedaio = "images/background/bedaio_v1.jpg"
image kate normal = "images/Kate/kate_v1.jpg"

#New character declaration
define ka = Character("Kate", color="#B03B03")

label bedaio_day1:

    scene bg black
    with fade

    "The drive from the airport was totally uninteresting. "

    scene bg bedaio
    with fade

    "But this, just wow. I heard that the school’s campus was situated in a nice spot, but I never imagined the view would be this expansive."
    "Well, I don’t want to waste any more time out here, they said I have to check-in at the administrative office by 12."
    "The school gate is wide open. It’ an ornate, rustic old thing. On either side of the gate the school’s walls are formidable, but further out they slim down to just a single layer of bricks."
    "They told me the front office should be easy to find, but I don’t know how I’m supposed to pick it out. The school-buildings all look like they’ve been placed haphazardly around the campus grounds."
    "My best bet is to make for the one closest to the gate. Even if it’s not the administrative center, I’m sure somebody will be able to pointme in the right direction."
    "The sleek, new age interior of ... this school building contrasts sharply with the olden-style exterior."
    "The floor is a shiny linoleum and and the walls look like they belong in a hospital. "
    "There’s nothing resembling a front desk or greeting area by the door so I push on deeper into the building. "
    "Every so often the off-white walls is broken by a wooden doorway. The plaques above the doorframes lead me to believe they’re allclassrooms."
    "I get to the end of the hall and realize I am totally lost."
    "At this point I’ll have to suck it up and just ask a teacher to point me in the right direction."

    show kate normal

    "I walk in a classroom door. It’s empty except for one girl seated at a massive table with her back to me."
    "The table is covered by a giant game board, some figurines and a bunch of notebooks. "

    s "Hi, I’m looking for-"

    p "An unusual voice, who would enter this bar so late at night?"

    "She leans back from the table and turns to look at me"

    p "He is too clean cut and well-dressed to be a roving drunk. Oh no, maybe he is the reason my comrades are delayed"
    p "Yes, he knew an operation was happening soon, so he blended in with the crowd and stayed on the lookout for passing Space Marines."
    p "He must have waylayed my comrades, killed them all, but for one. Then he tortured the last man until he revealed the location of ourmeeting place."
    p "No, but that doesn’t make sense. One of my men would never give information to an enemy of the Emperor, even under threat of a gruesomedeath."
    p "Traveler, who are you? What brings you here?"

    s "Uh, Hi I’m Suke. I’m lost I was looking for somebody that could give me directions to the administrative center."

    p "Ahh, so he’s just a lost traveler. A very rugged looking one at that. Well, the Emperor’s light has shined on me today."
    p "Suke, please come have a seat. "

    "Not knowing what else to do I walk across the classroom andsit beside her at the massive table."

    p "Suke, it is great to meet you, I am Imperator Kate of the 22nd"

    ka "Now we have some business to discuss! Let me tell you thestory so far. My comrades and I were sent to this planet by the Inquisitor Furiosa. "
    ka "The Inquisitor discovered that this planet harbored hereticsand traced the source to a section of this very town. "
    ka "Hard to believe, no? This town seems so caught up in love for the Emperor that heresy seems impossible. But heretics are very good at hiding their ways."
    ka "So, upon her discovery, The Inquisitor sent my squadmates and I to this planet to find and dispatch every last heretic. "
    ka "We selected this bar as our staging grounds for the assault on the heretics. We were supposed to meet here tonight, gear up and then moveon the heretics stronghold."
    ka "I do not know why, but for some reason my comrades have been greatly delayed. Gladly, their armor and armaments are stored here in the bar."
    ka "Suke, I will ask one thing of you. You seek information yes? I promise you that I will give you whatever information you need if you assistme in taking down these heretics tonight."
    ka "Do not be afraid, our mission is dangerous, but with the glory of the Emperor shining down on us we cannot lose."
    ka "So Suke, will you put your life on the line for the Emperor and me?"

    "Will I help Kate?"
    menu:
        "I have to go.":
            $ kateS = 0
            jump i_have_to_go
        "Yes Kate! I will fight with you for the glory of the Emperor!":
            $ kateS = 1
            jump i_will_fight

label i_have_to_go:

    "This is crazy. What is this girl talking about and why is she trying to bring me into it?"

    s "I have to go. They’re waiting for me at the administrative offices. "

    ka "Suke, you wound me. What is there to life but the fight for glory? I will let you go, but know that you have let down the Emperor and I."
    ka "You really do look like a great warrior though. Please consider joining the 40K club."

    s "Uhh, yeah sure."

    hide kate
    "I get up from the table and walk out the room. I’m back where I started. I’ll just go look through another building. "

    jump kate_1_rejoin

label i_will_fight:
    s "Yes Kate! I will fight with you for the glory of the Emperor!"

    ka "Then let us make last minute preparations. Here, I will show you how to don a suit of armor."
    ka "Need the Warhammer 40k person to write the Warhammer stuff"
    ka "Whew, that was the most fun I ever had. You did a great job, soldier!"

    s "You are an excellent leader. I think you could have done it without me. "

    ka "You pity yourself too much. Oh, you were looking for the administrative offices right? They’re just out the door on the right."
    ka "I’d take you there myself, but I have to clean up pieces. "

    s "Thanks Kate. How often do you guys run these campaigns?"

    ka "Twice a week, today and Wednesdays. You walked in right when we normally start."

    s "All right, I’ll try to show up Wednesday. See you later Kate!"

    hide kate

    "I take one final glance over the game board before getting up and walking out the door."
    "Oh no, now I can’t remember where Kate said the administrative offices are. "


label kate_1_rejoin:

    "Through sheer luck I manage to find the administrative building."

    scene bg black
    with fade

    "..."

    scene bg bedaio
    with fade

    "That was a whole lot of nothing. I thought they would at least give me a tour of the school. All they had me do was fill out some more forms, get my room key, and watch a presentation."
    "I stopped paying attention halfway through the “Introduction to Life at Bedaio” video."
    "As I exit the administrative building I notice it’s dark outside. It sure felt like I spent an eternity in there, but it really has gotten late. "
    "The campus looks so different at night. The haphazardly placed school-buildings block all the sightlines. From where I’m standing it looks like I’m in the middle of a gigantic maze. "
    "Thankfully they gave me a small map to help me navigate to my dorm. It’s only a short walk from where I am now. "
    "The school is eerily quiet, all the other students must have retired to their rooms already. "
    "As I pass by one building I hear a sound. Muffled music, a piano. It sounds amazing. I step around the side of the building towards the melody."
    "The building’s brick wall gives way to a sliding glass door. Looking inside I see neat rows of long tables laid out. It must be the school’s cafeteria."
    "In the middle of the room there is a break in the tables where a piano rests. A girl is sitting there in the dark playing. It’s too dark to make out her face but she has long hair and appears to be wearing a dress of some sort. "
    "From here I can hear her playing. It’s just beautiful. She commands the melody up and down. She plucks each key with the finesse of a professional pianist."
    "I find myself stopped outside the window staring at her. Eventually I snap back to my senses and continue on my way to the dorm rooms."
    "They said my room is on the right a few doors down from the entrance, #106. Here it is."
    "Luckily all my luggage has been left in the room ahead of me. All I have to do now is unpack and make my bed. I should probably get to sleep fast, It’s going to be a tough day tomorrow."

#First night
    scene bg black
    with fade

    "It’s going to be a tough day tomorrow."
    "It’s going to be a tough day tomorrow."
    "It’s going to be a tough day tomorrow."
    "?" "I will fight with you--???Through sheer luck???"
    "?" "Suke, you wound me."
    "¿" "?¿!¡!¡?¿!¡?¿!¡?¿"
    "It will probably be a tough day tomorrow,"
    "Suke"

    jump day_2


