#Script Start

## Declare characters used by this game. The color argument colorizes the name
## of the character.

define ik = Character("Ikarine", color="#000000")


#Image definitions
image mako timid = "images/Mako/mako_v1.jpg"
image mako normal = "images/Mako/mako_v1.jpg"
image nagamura normal = "images/Nagamura/nagamura_v1.png"
image ikarine normal = "images/Ikarine/ikarine_v1.png"

label day_3:
    
    "ZZZ"

    scene bg classroom
    with fade

    show nagamura normal

    na "-the electricity in the wire causes a magnetic field which then of course follows the right hand rule."
    na "And we can determine the force on a charged-"

    hide nagamura

    show nagamura at left

    show mako timid at right


    mm "Ummm, excuse me professor? Class ended five minutes ago and ..."

    hide mako

    na "Oh it did? How come nobody told me before? Is Mako really the only person in here with the self-confidence to tell me to shut up?"
    na "We'll continue our discussion on solenoids in the afternoon lecture. You're free to go."

    hide nagamura

    show mako normal

    mm "Come on Suke, let's hurry to lunch!"

    "Mako takes it for granted that we'll be going to lunch together. Some might find it annoying, but coming from her it's endearing."

    s "All right all right, just slow down!"

    hide mako

    scene bg campus_classroom
    with fade

    show mako normal
    with fade

    scene bg campus_cafeteria
    with fade

    scene bg cafeteria
    with fade

    mm "Whew, we made it in time. The line's not that long."

    s "I think you're seriously too afraid about being late to lunch."

    mm "Suke, there's nothing more important to maintaining a healthy body than good nutrition." 
    mm "Being late to lunch just once might take mintues off my life!"

    s "Isn't sitting in line already taking minutes off your life?"

    mm "But that's different!"

    "Mako and I get our food and go look for a table. As we're walking by Mako suddenly stops and says something under her breath."

    mm "Oh, she came to lunch today? Why didn't she tell me?"
    mm "Suke come on let me introduce you to a friend."

    "Mako leads me to the back corner of the lunch room. There aren't too many people sitting over here."
    "A lone girl sits at the far corner of the farthest table in the cafeteria. Mako motions me to sit down across from her as she goes around to the other side of the table."

    hide mako

    "I end up sitting down before Mako."

    show ikarine normal
    with fade

    s "Hi, I'm Suke, a friend of Mako's. "

    p "..."

    "Mako's made her way around the table and sits next to the quiet girl."

    hide ikarine

    show ikarine normal at left

    show mako normal at right

    mm "Ikarine this is Suke, he's a new transfer student."
    mm "Oh, and Suke, this is Ikarine. She's been going to Bedaio for the past four years just like me!"
    mm "We first met as first years. We've been good friends ever since."

    s "I see. Um ... Hi Ikarine."

    ik "..."

    mm "Ikarine's not very talkative. That's why she and are such good pals!"
    mm "Let's dig in."


    "I can't take my eyes off this peculiar girl as I eat my cafeteria food."
    "She seems totally unperturbed by my glare. Come to think of it, she didn't seem to notice Mako or myself at all."
    "It's as if she's still eating alone. Maybe in her mind she is."

    s "So Ikarine, what class are you in? I bet your professor's a lot more lively than Nagamura."

    ik "..."
    ik "..."
    ik "..."

    show mako startled at right
    with fade

    mm "Iki's~ in Class 3-3. Thats Obediah's class. He's a westerner."

    s "He's not one of those otaku from out of the nation is he? Those guys are always creeps."

    ik "... he's and ex-pat from World War two. He killed four Japanese during the war and hated himself ever after."
    ik "... He's been teaching high school classes in Japan for the past 50 years."

    s "Wow, I-I'm sorry. I mean, what a great guy."

    ik "He killed four Japanese boys in World War two."

    s "..."
    s "I mean, I'm sure he ... I guess he did."

    ik "..."

    mm "Hey guys, this chicken is great yeah right?"

    #Put both of these lines together.
    s "I got the pork."
    ik "I got the pork."

    s "..."
    ik "..."

    mm "Ha! you guys are so similar nyaa~ Mako knew it was a good idea to introduce you two."
    mm "Oh no, look at the time. Lunch is almost over! Mako is going to have to eat fast if she wants to be healthy."

    hide mako
    hide ikarine

    "The rest of the meal passes in relative silence as the only talkative person at the table is enamored with her meal."
    "I'm so put off by Ikarine that I barely manage to eat a couple more bites of pork before lunch is over."

    show mako annoyed
    with fade

    "Mako looks at me disapprovingly as we clean our plates."

    show mako shocked
    with fade

    "Oh, I have to hurry back to class. Mako almost forgot she has to ask Nagy~ something about the festival."

    hide mako

    s "So Ikarine, umm-"

    ik "Do you want to skip class?"

    s "Wha-"

    ik "Do you want to skip class with me? You said Nagamura is boring so I assume you don't want to go back to class."

    menu:
        "Yeah you're right. Nagamura's boring I shouldn't waste my time in that class.":
            $ ikarineS += 1
            jump skip_class
        "I don't think it's a good idea to skip class so early after joining.":
            jump dont_skip

label skip_class:
    s "Yeah you're right, skipping class is a great idea. Nagamura's boring I shouldn't waste my time in that class."

    ik "Ok, so what are we going to do?"

    s "..."
    s "You don't have any idea?"

    ik "No."

    s "You have no idea where you want to go, but then why did you suggest skipping class?"
    s "I mean, it's just, why do you want to skip class?"

    ik "I don't want to skip class, you want to."

    s "But you were the one to suggest it!"

    ik "I asked if you wanted to skip class and you said yes."

    s "Ok ok, but ...."
    s "Nevermind. Well, uhh, does this school have I don't know. Some place to just relax?"

    ik "Sure."
    ik "..."

    s "Well can we go there?"

    ik "Which one?"

    s "Which what?"

    ik "Which relaxing place do you want to go to?"

    "Our discussion is interrupted by the bell signalling the start of afternoon classes."

    s "I don't know, any of them!"

    ik "Ok."

    hide ikarine

    "Ikarine turns on her heels and walks out the cafeteria door. It takes me a second to gather myself before I can go out after her."

    scene bg campus_cafeteria
    with fade

    show ikarine normal
    with fade

    "The campus is empty except for a few students running to their classes."
    "Conversely, Ikarine and I are just strolling along without a care in the world."

    scene bg campus_1
    with fade

    s "Are we going off campus?"

    ik "No, we're almost there."

    scene bg campus_dormitory
    with fade

    "Ikarine's seemingly aimless paragrinations have taken us by one of the female dormitories."
    "It doesn't look all too different from my dorm."

    scene bg dormitory
    with fade

    "And now we're inside the female dorms."

    s "Ikarine, where exactly are you taking us?"

    ik "To a relaxing place. That's where you wanted to go right? You said any place is fine so I picked one at random."

    s "But specifically, which relaxing place are we going to?"

    ik "My dorm room."

    "I don't know what I was expecting. My elation at getting to skip classes made me momentarily forget how weird Ikarine is."
    "But now it's all coming back to me."

    s "Are you sure it's a good idea to bring a boy you barely know to your dorm room the same day you meet him?"

    ik "..."
    ik "I'm just taking us to a relaxing place. Do you think it's not going to be relaxing?"

    s "That's not what I mean-"

    ik "Well we're here."

    "Ikarine nonchalantly pulls out her keys and unlocks her door."
    "That reminds me, I didn't lock my door this morning. I hope nobody gets any strange ideas."

    hide ikarine

    "Ikarine's already stepped through the doorway into her room."

    ik "Pleae close the door behind you. It's not very relaxing when the door is open."

    "I hesitate for a moment before following her inside. In my distracted state I almost forget to close the door."

    scene bg room_ikarine
    with fade

    show ikarine normal
    with fade

    "Ikarine's room is the definition of austere. The only furnishings are the pre-supplied bed, desk, chair, nightstand, and corner lamp."
    "Her walls are devoid of posters or other ornamentation."
    "Even her desk is bare except for a backpack placed on top."
    "Ikarine herself is lying on the floor next to her bed."

    ik "I normally relax on my bed rather than the floor, but I thought it would be more proper to offer you the more relaxing location."

    s "I don't know what to say. Thanks I guess?"

    "I remain motionless by the door."

    ik "You don't look very relaxed. Are you not going to use the bed? If I'd have known you were just going to stand the whole time I'd have used the bed myself."

    s "Yeah, I think that's a good idea. You can use the bed."

    "Ikarine mechanically pulls herself up from the floor and onto her bed. She again resumes a totally relaxed position lying on her back."
    "I still can't muster the self control to move from the door."

    ik "Ok I'm on the bed now. Please stop standing by the door. You're never going to relax that way."
    ik "The floor is very comfortable, I know, I was just there."

    "I half get down, half fall to the floor. I don't feel any more relaxed."

    ik "Now, let's just relax."

    "I can't see Ikarine anymore from my position but I can sense that she's closed her eyes. Out of respect I follow suit."

    #Insert some sort of time delay here. 

    "After a while I do indeed feel much more relaxed. I've almost forgotten that I'm in a complete stranger's room, lying on her floor and staring up at her ceiling."
    "The only indication I'm not alone in the room is Ikarine's quited breathing."

    #Another time delay

    "Why was I so worried to begin with? This is just, just relaxing."
    "I'm sitting on the floor, doing nothing, worrying about nothing, thinking about nothing."
    "I have no problems, no worries, no cares."
    "Nobody around, nobody speaking, nobody listening, nobody caring."
    "Nothing."
    "Just nothing."

    scene bg black
    with fade

    "Nothing at all."

    #Do some sort of waking up animation

    "aaahhhhhhhhh."
    "Where am I?"
    "Oh right, I'm in new room in Bedaio."
    "But why does my back hurt so much?"
    "Wait, why am I on the floor?"

    show ikarine sleeping

    "Who is this on the bed!?"
    "Oh yeah, I'm not in my room. I went to some girl's room."
    "Did I just sleep with this girl?"
    "Think Suke, think. Ok, This is, she's Ikarine. And what were we doing?"
    "I'm fully clothed so it couldn't be ... Yes, we were just relaxing."
    "Just relaxing."
    "And it looks like she's <i>still</i> relaxing."
    "Would it be rude to wake her?"
    "She looks so peaceful."
    "What time is it anyway? I'll let myself out."

    scene bg dormitory
    with fade

    "I'm in the girl's dormitory. I hope it's not after hours. I can't imagine what they'd do to a guy wandering around the girl's dormitory after hours."
    "Was it dark outside? Ikarine had a window in her room, but yeah it was dark in her room."
    "Oh jeez, it must be pretty late. I have to get out of here quick."

    scene bg campus_dormitory
    with fade

    #This only happens with lisa at 1 because Suke internally is only going to the cafeteria with the hope that he might see Lisa.
    if lisaS > 0:
        "Great, now I'm wide awake and it's dark out. Might as well go study in the cafeteria again. Get Nagamura's homework for next week done."

        scene bg campus_cafeteria
        with fade

        show lisa nervous
        with fade

        s "Oh, hi Lisa! You done practicing for the night?"

        li "S-S-Suke, hi. Y-Yes, I'm done playing."

        s "That's too bad, I was just about to go do some studying myself."

        show lisa startled
        with fade

        li "Have, h-have a nice time studying!!"

        hide lisa

        "Well that was the typical Lisa all right."

        scene bg cafeteria
        with fade

        "Okay Nagamura. Week 1 was child's play, let's see if you've stepped up your game for week 2."

        scene bg black
        with fade

        jump day_4


    scene bg campus_1
    with fade

    scene bg campus_dormitory
    with fade

    scene bg dormroom
    with fade

    "Home at last. And it's only 11 o'clock at night! Great, just great. How am I going to go to bed now when I've already gotten a full night's worth of sleep?"
    "And Ikarine was <i>still</> sleeping when I left. How can somebody sleep so much and yet be so out of it when they're awake?"
    "Now what can I possibly do so late at night?"
    "Might as well get started on next week's homework. I don't have anything better to do."

    scene bg black 
    with fade

    jump day_4



label dont_skip:

    s "I don't think it's a good idea to skip class so early after joining."
    s "I'll speak to you later Ikarine."

    ik "That's disappointing."

    hide ikarine

    "With that we go our seperate ways to class."

    scene bg black
    with fade

    scene bg classroom
    with fade

    show nagamura normal
    with fade

    na "-and that concludes today's lecture on solenoids."
    na "Before you all leave, I'd like to remind you about the upcoming club fair."
    na "A few clubs will be hosting their booths in our classroom so if you have any belongings in here you'd best remove them before friday."
    na "I hope to see you all participating in some way. Clubs keep the school lively and intersting. Every student should be part of at least one club. Have a nice day."
    
    hide nagamura

    "I can't help but think that last jibe was directed at me. I'm probably the only kid in the whole school that's not part of a club."
    "Although I have a pretty good reason for that."

    scene bg campus_classroom
    with fade


    mm "Suke! Wait up!"

    "I turn around to see Mako bursting out of the classroom doors."

    show mako winded
    with fade

    mm "Suke, you know *pant* it's ru-ru--"

    show mako normal
    with fade

    mm "It's rude to walk out of class before saying 'bye' to your friends!"
    mm "Bye Suke!"

    s "bye."

    "Mako acknowledges my tepid response before turning to walk away."

    show mako back
    with fade

    mm "I'd love to hang out nyaa~ but Mako has to go do work."

    hide mako

    "My dorm's in the same direction Mako's walking. It'd be too awkward to go that way now that we've already said bye."
    "I'll just take the long way around."

    scene bg campus_1
    with fade

    "Students are walking across campus every which way. Probably all going to their clubs."
    "Meanwhile I'm just awkwardly taking the long way to my room. Why didn't I just follow Mako?"

    if kateS >= 1:
        "Suddenly I spot a familiar face in the crowd."

    else:
        "Suddenly I see a vaguely familiar face in the sea of strangers."

    "My momentary hesitation appears to have caught her attention and she's begun to walk towards me with determination."

    if kateS >= 1:

        ka "Suke my old friend it is great to see you again and at just the right time."

        s "Oh Kate, hi."

        ka "The 40K club is having it's next meeting right now. We need you to help us!"
        ka "We have heard whisperings of another den of heresy right within the same sector. Word is that they are planning a massive chaotic ritual."
        ka "The Emperor save us all if we don't manage to stop them."

        s "Well, I don't know, I have a lot of work to do."

        if lisaS >= 1:

            "What are you saying Suke, you know you've finished all your work for the week."

        menu:

            "But I will put it all off to fight with you for the glory of Empire!":

                $ kateS += 1
                jump fight_again

            "I'm sorry, but I won't be able to play with you again.":

                jump dont_fight_again

    else:

        ka "Is that who I think it is? Indeed I am correct. Suke I picked you out as a fighter the moment I saw you."

        s "Huh, who are you?"

        ka "I am the Grand Imperator Kate Retorium."
        ka "We met in the inn not two nights ago. You turned me down in a time of dire need."

        s "Oh, you were in the table top club."

        ka "Correct Suke. Now I may have been able to succeed without your help in the last fight, but this new den of heresy puts the old one to shame!"
        ka "I will surely be defeated without your much needed assistance."
        ka "So what say you Suke? Will you fight with me? To the death if need be?"

        s "I don't know, I have a lot of work to do."

        if lisaS >= 1:

            "What are you saying Suke, you know you've finished all your work for the week."

        menu:

            "But I will put it all off to fight with you for the glory of the Empire!":

                $ kateS += 1
                jump fight_again

            "And I don't really like tabletop games. Count me out.":

                jump dont_fight_again

label fight_again:

    s "But I will put it all off to fight with you for the glory of Empire!"

    ka "Excellent, now let's go meet our compatriots. I believe they are already waiting at the inn."

    #Do a bump or something

    me "My darling Kate, have you found another mark for your sham of a story?"

    show kate normal at left

    show megumi smug at right
    with fade

    #Met with megumi at school. Hung out after school.
    if megumiS >= 1:

        s "Hi Megumi"

        show kate angry at left

        ka "Megumi, you stay out of this."

        me "Suke, don't tell me, has Kate really convinced you to join that silly little club of hers?"

        s "I'm not a part of any club. I'm just there to help with the story."

        me "'Story', are you referring to that listless swill that Kate cooks up in that crazy head of hers?"
        me "Trust me, you don't want to get involved in any of that. I couldn't imagine anything more spectacularly uninteresting."
        me "But please, I don't say that to slight you in any way. Even if you've been tricked by this charlatan you are still a very interesting man."
        me "I truly do hope you will find some minimal amount of fun in Kate's little games."
        me "Bye bye now!"

        hide megumi

        ka "Megumi, I don't have to sit here and take your abuse. Come on comrade, we have work to do."

    #Haven't met with megumi yet
    else:

        show kate angry at left

        ka "Get out of here Megumi, I don't need you insulting me and my club members."

        me "You've convinced this misled young lad to join your club so soon? You must have stepped up your sales pitch."

        s "Just wait a minute here. I'm not part of any club yet, and please whoever you are, there's no need to insult Kate."

        show megumi surprised at right
        with fade

        me "Wait a moment, you look oddly familiar. Oh yes it's the Not Runaway!"

        s "'Not Runaway'? ..."
        s "You're the girl from the plane! I thought I saw you wandering around campus earlier. I didn't realize you were such a jerk."

        me "And to think I had such high hopes for you. Well, all is certainly not lost. I'm sure once you see the true nature of Kate's 'story' you'll come to your senses."
        me "Bye bye now! Do try to have fun with your little charade."

        hide megumi

        ka "Come on Suke, let's go find out compatriots."


    scene bg 40k_room
    with fade

    "Some wh 40k stuff"

    ka "All right Space Marines, I'll see you all the same time two days from now!"

    if kateS >= 2:

        ka "Hey, Suke wait up a minute."

        s "What do you need Imperator?"

        show kate timid
        with fade

        ka "I was just wondering..."
        ka "What class are you in?"

        s "Oh, I'm in Nagamura's class."

        show kate normal
        with fade

        ka "Excellent that is right by mine. Shall we meet for lunch tomorrow?"

        s "Gladly! It will be a fine time dining with you Imperator."

        show kate happy
        with fade

        ka "Well then, 'til tomorrow."

        s "See you later, Kate."

        hide kate

        scene bg campus_1
        with fade

        "That was pretty unusual for Kate, I can't tell if she was still in character or if ..."
        "If what Suke?"

        scene bg black
        with fade

        jump night_3        

label dont_fight_again:
    
    if kate == 0:

        "Why does this girl keep bugging me about playing her silly games? Doesn't she get that I have no interest in that kind of stuff."
        "The students around bustling to and fro from club to club."
        "The birds chirping in the surrounding trees."
        "The gentle hum of an air-conditioner on a nearby roof."
        "The noises all around me create a cacaphonous din of discomfort."
        "And this girl in front of me stands at the center of it all."
        "Like some great maestro directing a concerto of chaos."
        "I want nothing to do with her."

        s "Count me out of your silly board games."
        s "And please don't ever ask me again."

        ka "I, understand ..."
        ka "Some people are just not cut out for these types of things."
        ka "Well, 'til we meet again Suke."

        s "Have fun with your games."


    else:

        "The first time was ok, but I don't think Warhammer roleplaying is something I want to focus on right now."
        "There's more interesting clubs to join and more productive things to do than fantasize for hours a day."

        s "Kate, I don't think I'm going to be able to play with you today. Maybe some other time."

        ka "That's understandable. Of course, you're not a member of our battalion yet so it's unreasonable to expect you to join us in every engagement."
        ka "'til we meet again Suke."

        s "Later."

        me "My darling Kate, have you found another mark for your sham of a story?"

        show kate angry at left

        show megumi smug at right

        ka "He's not a 'mark', Megumi. And no, he isn't joining the 40K club today."

        if megumiS >= 1:

            s "Megumi? You know Kate?"

            show megumi normal at right

            me "Suke, I didn't realize it was you. So you've seen through Kate's ruse, eh?"
            me "You saved yourself from a lot of boredom."
            me "Oh what am I doing, you've already lost Kate, there's no need to put you down any further."

            ka "I better get going, My Battallion is probably eagerly awaiting my leadership."
            ka "See you later Suke. Megumi."

            s "Later Kate."

            hide kate

            s "Megumi, what's all this with Kate?"

            me "Oh I was just making fun of her shoddy craftsmanship. Criticism only works for good you know."
            me "Kate's been trying to gain some new club members for a while now, but she's going about it in all the wrong ways."
            me "To gain more members all she has to do is make a better story."

            s "I didn't notice any glaring flaws in her story the first time I played."

            me "You played before?"

            s "Yeah, the first day I was here. I had a fun time."
            s "But maybe you're right, it wasn't boring, but it wasn't anything special."

            me "Suke, 'not anything special' is the harshest criticism any piece of writing could ever receive."

            "Really? I'm pretty sure, 'a pile of dogshit' sounds a lot harsher."

            me "Well then, since you're not doing anything now, do you want to join me for another meal down in town?"
            me "We won't go to such a fancy restaurant, but this time I don't think I'll be footing the whole bill."

            s "Sure, it's not like I'm struggling with school work or anything."

            jump megumi_dinner_2

        s "Megumi? you look oddly familiar."

        me "And you as well ... Ah yes the Not Runaway we meet again. I had a sneaking suspicion that you were going to attend Bedaio."

        s "'Not Runaway'? Wait a minute, you're the girl from the plane! I thought I saw you wandering around campus earlier. I didn't realize you were such a jerk."

        me "I knew I saw something in you. It takes a good head on your shoulders to see through Kate's ruse. Her 'story', if you can even call it that, is boring swill."
        
        ka "I have no time to sit here and listen to this abuse. I must be off to help my batallion."
        ka "Suke, I'll talk to you later. Megumi ... I hope we don't talk again any time soon."

        me "The feeling is mutual."

        hide kate

        "That was pretty cold. There's obviously something going on between those two."

        me "So Not Runaway, excuse me, I mean So Suke how do you know Kate?"

        s "I met her the first day I was in Bedaio, she was the first student I--Actually, I guess you're the first student I met."
        s "To be honest, you didn't leave that good of a first impression."

        show megumi wounded
        with fade

        me "Oh, you wound me so Suke. And to think of all the good words I said about you earlier."
        me "Truly, you have hurt me in a way I know not how I can be fixed."

        "She's laying it on thick. I didn't think human beings could even be this ironic."
        "Might as well play along seeing as I have nothing better to do."

        s "I am ever so sorry Megumi. I didn't realize how highly you regard my opinion of you."
        s "However can I make it up to you?"

        show megumi smug
        with fade

        me "Well there is this restaurant in town that I've always wanted to go to. It's an, how do they call it, 'Italian Place?'"

        "The sun is shining brightly, the birds are chirping in the trees, the temperatures not too cold nor too hot but just right."
        "There couldn't be a better day to go out to town, especially for my first trip. It'd be a crime to turn down this opportunity."

        s "Sounds like a plan."

        me "Lovely! Shall we do go now?"

        s "Lets."








#Shoudl you really include this dinner? 
label megumi_dinner_2:
    

label night_3:
    




