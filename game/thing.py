from parser import HTMLParser

global s
s = ""
class MyHTMLParser(HTMLParser):
	def handle_starttag(self, tag, attrs):
		self.option = 'false'
		if (tag == "p"):
			self.option = attrs[0][1].upper()
	def handle_endtag(self, tag):
 		self.option = 'FALSE'
	def handle_data(self, data):
		global s
		if self.option == 'CHARACTER':
			self.character = data[0:1]
		if self.option == 'DIALOGUE':
			s += self.character + " \"" + data.replace("\n"," ") + "\"\n"
		if self.option == 'MSONORMAL':
			s += "\"" + data.replace("\n"," ") + "\"\n"
parser = MyHTMLParser()
parser.feed("""<p class=MsoNormal>I arrive at the front gate exactly on time. I see Megumi
walking up a little bit after me. </p>

<p class=CHARACTER>megumi</p>

<p class=Dialogue>Ah, you’re a little early. You wanted to make sure you
weren’t late but you didn’t want be too early because of all the things that
would implicate. </p>

<p class=CHARACTER>suke</p>

<p class=Dialogue>Actually, I’m perfectly on time, you’re the one that’s late. </p>

<p class=CHARACTER>megumi</p>

<p class=Dialogue>I don’t see how I could be late to a meeting that I set the
time for. </p>

<p class=CHARACTER>suke</p>

<p class=Dialogue>How far is the bookstore?</p>

<p class=CHARACTER>megumi</p>

<p class=Dialogue>It’s at the edge of town closest to Bedaio, once we get down
the mountain we’ll be nearly there. A twenty minute walk tops. </p>

<p class=Dialogue>Our dining location is just a little bit beyond the
bookstore. Another ten minute walk. </p>

<p class=CHARACTER>suke</p>

<p class=Dialogue>Oh, you already picked out where we are eating? What if I
have some special diet? Maybe I can only east sushi?</p>

<p class=CHARACTER>megumi</p>

<p class=Dialogue>Maybe I picked a sushi shop. </p>

<p class=CHARACTER>suke</p>

<p class=Dialogue>What if I’m allergic to seafood?</p>

<p class=CHARACTER>megumi</p>

<p class=Dialogue>Maybe I didn’t pick a sushi shop. And I would feel very sorry
for you if you couldn’t eat seafood. The consumption of sushi is one of life’s
greatest pleasure.</p>

<p class=CHARACTER>suke</p>

<p class=Dialogue>Indeed it is. So when are we going to get to the bookstore?</p>

<p class=CHARACTER>megumi</p>

<p class=Dialogue>We’re still on the mountain. When we are off the mountain we
will be near the bookstore. I’ve already told you this once Suke, why must you
bore me further with incessant questions about our destinations?</p>

<p class=CHARACTER>suke</p>

<p class=Dialogue>I’m just making conversation. </p>

<p class=MsoNormal>Megumi and I speak little more as we continue down the
mountain. Eventually we get to the bottom, and as Megumi promised the bookstore
is right there. </p>

<p class=MsoNormal>Walking into the store I can see the table setup for
Megumi’s book signing. Beside the table is a stack of books and behind Megumi’s
seat there’s a large banner with her name on it.</p>

<p class=CHARACTER>suke</p>

<p class=Dialogue>Wow, you really are a celebrity aren’t you.</p>

<p class=CHARACTER>megumi</p>

<p class=Dialogue>Wait until you see how many people show up before you offer a
remark like that. </p>

<p class=Dialogue>Wait here a second, I’m going to ask the store manager to
bring out another seat. </p>

<p class=Dialogue>You can look over the store’s selection I guess. Oh, and they
sell Bedaio schoolbooks if you’ve yet to buy yours. </p>

<p class=MsoNormal>I already have all my textbooks so I make my way to the
science fiction section. They offer a wide array of novels. I find myself
browsing from title to title without giving any book a chance. </p>

<p class=MsoNormal>I eventually find a book that piques my interest. It’s about
a human raised by Martians and his struggles after coming to Earth for the
first time. </p>

<p class=MsoNormal>The writer’s style is incredibly engaging. I read the first
twenty pages standing in front of the bookshelf. I’m not sure how much longer I
would have been stuck there if Megumi didn’t wave me over to her table. </p>

<p class=MsoNormal>We both take our seats at the table, Megumi front and center
with me covering her left flank. I quickly re-engross myself in my book. </p>

<p class=MsoNormal>Soon enough groups of patrons queue up in front of our table
and Megumi begins signing her autograph at a rapid pace.</p>

<p class=CHARACTER>megumi</p>

<p class=Dialogue>Thank you so much for your interest in my work. I appreciate
each and every reader. Be sure to look for my upcoming book “A Stranger Life”
coming out in December!</p>

<p class=MsoNormal>She repeats the same line verbatim to each person that
brings her a book to sign. </p>

<p class=MsoNormal>I begin to drift out of our reality and into the designed by
the novel resting in my hands. The constant drone of Megumi and her fans serve
as an incantation to drive me deeper into a world of fantasy.</p>

<p class=MsoNormal>After another seventy five pages or so I am dragged back to
reality by a hand on my shoulder.</p>

<p class=CHARACTER>megumi</p>

<p class=Dialogue>Hey Suke are you here? Earth to Suke, beep boop.</p>

<p class=CHARACTER>suke</p>

<p class=Dialogue>Oh, wow, you can say something else besides “Be sure to look
for my-</p>

<p class=CHARACTER>megumi</p>

<p class=Dialogue>All right, all right. A lot of hlp you were. You were
supposed to be my distraction, liven things up when it got too boring. I forgot
you were even sitting next to me by the end of it.</p>

<p class=CHARACTER>suke</p>

<p class=Dialogue>Sorry, I got really engrossed in this novel. </p>

<p class=CHARACTER>megumi</p>

<p class=Dialogue>Let me see, yes, that’s a classic, really defined sci-fi in
the 70s-80s. Well come on. I’ll meet you by the entrance, I just have to say
bye to the storeowner.</p>

<p class=MsoNormal>Megumi gets up and walks off to the back of the bookstore. I
grab my book and make way for the entrance. </p>

<p class=MsoNormal>When Megumi’s done talking she and I leave the store and
head off through town in a path chosen exclusively by her. We stop outside a
small restaurant in the middle of a block. </p>

<p class=MsoNormal>We walk through the entrance before I think to read the
restaurant’s name. The lighting inside is significantly dimmed. I can barely
make out the hostess. </p>

<p class=MsoNormal>Apparently Megumi’s eyes are quicker to adjust than mine and
she takes the lead with the hostess. I find myself following along from the
back. </p>

<p class=MsoNormal>When we are seated my eyes finally grow accustomed to the
dim light.</p>

<p class=CHARACTER>suke</p>

<p class=Dialogue>&nbsp;This place is really fancy. I don’t think I’ve ever
been in a restaurant like this before. </p>

<p class=CHARACTER>megumi</p>

<p class=Dialogue>It’s an Italian place. Not very common. I love Italian food so
having this place close to campus is a godsend. I try to eat here at least once
a week. The food is just so good. </p>

<p class=CHARACTER>suke</p>

<p class=Dialogue>How did you develop a taste for Italian food?</p>

<p class=CHARACTER>megumi</p>

<p class=Dialogue>That’s a long, uninteresting story. I’ll tell you sometime
later when we don’t have anything more interesting to talk about. </p>

<p class=MsoNormal>We’re interrupted when the waitress takes our drink orders. I
just get water, but Megumi orders something with an English name.</p>

<p class=CHARACTER>suke</p>

<p class=Dialogue>An “Ahnuld Pahner”? What’s that?</p>

<p class=CHARACTER>megumi</p>

<p class=Dialogue>Hahaha, I ordered an “Arnold Palmer”. It’s just ice tea and
lemonade. </p>

<p class=CHARACTER>suke</p>

<p class=Dialogue>How did the waitress know what you meant?</p>

<p class=CHARACTER>megumi</p>

<p class=Dialogue>I told you, I eat here at least once a week. It would be
shocking if she didn’t know what an Arnold Palmer is by now. </p>

<p class=CHARACTER>suke</p>

<p class=Dialogue>You really eat here once a week? Doesn’t that get, expensive?
This place looks very fancy.</p>

<p class=CHARACTER>megumi</p>

<p class=Dialogue>Don’t worry Suke, I’ll pick up the tab.</p>

<p class=CHARACTER>suke</p>

<p class=Dialogue>No, that’s not what I meant, just nevermind. </p>

<p class=CHARACTER>megumi</p>

<p class=Dialogue>I know what you meant. Suke, I’m an author with books in
stores over half of Japan. I would eat here every day if schoolwork and my
writing allowed it. </p>

<p class=CHARACTER>suke</p>

<p class=Dialogue>I’m sure you’d get sick of Italian food if you ate here every
day. </p>

<p class=CHARACTER>megumi</p>

<p class=Dialogue>There’s a lot of variety. Here, stop talking to me and check
out the menu, I want order when the waitress brings our drinks. </p>

<p class=MsoNormal>I open the menu for the first time and read it over. I can’t
tell if there’s any variety or not because I have no idea what anything is on
this menu.</p>

<p class=CHARACTER>suke</p>

<p class=Dialogue>Umm Megumi, I’ve never eaten Italian befo-</p>

<p class=CHARACTER>megumi</p>

<p class=Dialogue>Oh yeah, what was I thinking. Don’t worry, I’ll order for
you. </p>

<p class=CHARACTER>suke</p>

<p class=Dialogue>Try not to get me anything too out there. What’s the typical
Italian fare? Pizza and noodles right?</p>

<p class=CHARACTER>megumi</p>

<p class=Dialogue>Yes, but not they’re not normally served together. Just wait
and see. </p>

<p class=MsoNormal>I continue to look over the menu even though I won’t be
ordering anything. Eventually the waitress comes back with our drinks and takes
our orders. </p>

<p class=CHARACTER>megumi</p>

<p class=Dialogue>Thank you, we’ll be ordering now. I’ll have the pasta
pomodoro.... Yes and I’ll be ordering for him as well. He’ll have the lasagna
frita. </p>

<p class=CHARACTER>suke</p>

<p class=Dialogue>Ooh, a “Lasagna Frite” that sounds fancy. </p>

<p class=CHARACTER>megumi</p>

<p class=Dialogue>It’s just some fried pasta, meat sauce and cheese.</p>

<p class=CHARACTER>suke</p>

<p class=Dialogue>Well then I guess I’ll have to try some of your Pasta
Pomodoro. We can share right? That’s not like frowned upon in Italian
restaurants is it?</p>

<p class=CHARACTER>megumi</p>

<p class=Dialogue>Sure we can share, at forty to twenty. </p>

<p class=CHARACTER>suke</p>

<p class=Dialogue>Forty to twenty what?</p>

<p class=CHARACTER>megumi</p>

<p class=Dialogue>I get forty percent of your meal and you get 20 percent of
mine. </p>

<p class=CHARACTER>suke</p>

<p class=Dialogue>Whoah whoah whoah, that doesn’t sound horribly fair. How
about thirty to thirty. </p>

<p class=CHARACTER>megumi</p>

<p class=Dialogue>I’ll go twenty to thirty. </p>

<p class=CHARACTER>suke</p>

<p class=Dialogue>Come on, I’m the bigger one here, I need more sustenance than
you do. I should be the one arguing for a larger portion. </p>

<p class=CHARACTER>megumi</p>

<p class=Dialogue>Your body may require more nutrition than mine, but writing
requires a lot of mental effort. I’m sure I burn at least three times the brain
calories that you do.</p>

<p class=CHARACTER>suke</p>

<p class=Dialogue>Well regardless, I’m not going any higher than thirty to
thirty. </p>

<p class=CHARACTER>megumi</p>

<p class=Dialogue>Ok, but you have to give me an extra breadstick. </p>

<p class=CHARACTER>suke</p>

<p class=Dialogue>Deal</p>

<p class=MsoNormal>With that settled the waitress arrives bringing our food.</p>

<p class=MsoNormal>Megumi hastily eats as much of my meal as she eats of her
own. It actually disappoints me a bit as I’m really enjoying it. </p>

<p class=MsoNormal>Despite my best efforts Megumi comes out on top with a solid
forty to thirty lead. </p>

<p class=CHARACTER>suke</p>

<p class=Dialogue>That was fantastic</p>

<p class=CHARACTER>megumi</p>

<p class=Dialogue>Eh, the pasta was a little dry.</p>

<p class=MsoNormal>The waitress comes back one final time to take our plates
away and give us the check. Before I am able to even reach over Megumi has her
credit card out and hands it to the waitress. </p>

<p class=CHARACTER>megumi</p>

<p class=Dialogue>You jumped a little there, Suke. Have you never had your meal
paid for by a girl?</p>

<p class=CHARACTER>suke</p>

<p class=Dialogue>Just my mother. And then only very reluctantly.</p>

<p class=CHARACTER>megumi</p>

<p class=Dialogue>I’m sure she was happy to do it. Your company has more than
made up for your meal. </p>

<p class=CHARACTER>Suke</p>

<p class=Dialogue>Being this charming takes a lot of work you know. </p>

<p class=CHARACTER>megumi</p>

<p class=Dialogue>For you, I’m sure it does.</p>

<p class=MsoNormal>Megumi and I get up from our table and walk out the front
door. Enough time has passed that our eyes don’t need to adjust too much to the
sunlight.</p>

<p class=MsoNormal>We exert ourselves walking back to school much more than we
did on the way down. I guess that’s how it is with schools on hills.</p>

<p class=MsoNormal>We finally make it back to campus just as the sun is
beginning to set. </p>

<p class=CHARACTER>megumi</p>

<p class=Dialogue>Thanks for coming with me Suke. Going out like this is a lot
more fun when it’s with an interesting person. </p>

<p class=CHARACTER>suke</p>

<p class=Dialogue>I agree, see you tomorrow?</p>

<p class=CHARACTER>megumi</p>

<p class=Dialogue>Time will tell. Good night, Suke.</p>

<p class=MsoNormal>With that Megumi walks off to one of the girl’s dormitory
sections. I make my own way back to my dormitory. </p>
""")


f = open("foo.txt","w")
f.write(s)
f.close()